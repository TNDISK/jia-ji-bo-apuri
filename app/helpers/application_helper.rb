module ApplicationHelper
    def full_title(title = "")
        if title.empty?
            "家計簿アプリ"
        else
            title + " | 家計簿アプリ"
        end
    end
end
