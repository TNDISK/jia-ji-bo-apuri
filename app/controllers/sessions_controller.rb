class SessionsController < ApplicationController
  before_action :ban_login_user, only: [:new, :create]

  def new
  end
  
  def create
      user = User.find_by(email: params[:session][:email])
      if user && user.authenticate(params[:session][:password])
          login(user)
          current_user
          flash[:notice] = "ログインしました"
          redirect_to root_url
      else
          flash.now[:notice] = 'メールアドレスまたはパスワードが間違っています'
          render 'new'
      end
  end
  
  def destroy
    session[:user_id] = nil
    flash[:notice] = "ログアウトしました"
    redirect_to login_path
  end
  
  private
    def ban_login_user
      if session[:user_id]
        flash[:notice] = "すでにログインしています"
        redirect_to root_path
      end
    end
end
