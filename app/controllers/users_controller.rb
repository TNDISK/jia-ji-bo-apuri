class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:show, :edit, :update, :destroy]
  before_action :correct_user, only: [:show, :edit, :update, :destroy]
  before_action :ban_login_user, only: [:new, :create]

  def new
    @user = User.new
  end

  def show
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
        session[:user_id] = @user.id
        flash[:notice] = "ユーザーを作成しました"
        redirect_to @user
    else
        render 'new'
    end
  end

  def edit
  end
  
  def update
    if @user.update_attributes(user_params)
      flash[:success] = "プロフィールを変更しました"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def destroy
     @user.destroy
     flash[:notice] = "ユーザーを削除しました"
     redirect_to root_path
  end
  
  private
    def set_user
       @user = User.find(params[:id])
    end
    
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end
    
    def correct_user
      if current_user.id != params[:id].to_i
        flash[:notice] = "権限がありません"
        redirect_to root_url
      end
    end
    
    def ban_login_user
      if session[:user_id]
        flash[:notice] = "すでにログインしています"
        redirect_to root_path
      end
    end
end
