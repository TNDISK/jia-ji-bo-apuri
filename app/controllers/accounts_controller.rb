class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user
  before_action :correct_user_account, only: [:show, :edit, :update, :destroy]


  def index
    @accounts = Account.where(user_id: current_user.id).search(params[:search])
    sum
  end

  def new
    @account = Account.new
  end

  def create
    @account = current_user.accounts.build(account_params)
    change_account
    if @account.save
      flash[:notice] = "作成しました"
      redirect_to @account
    else
      render 'new'
    end
  end

  def show
  end

  def edit
  end
  
  def update
    change_account
    if @account.update(account_params)
      flash[:notice] = "編集しました"
      redirect_to @account
    else
      render 'edit'
    end
  end
  
  def destroy
    @account.destroy
    flash[:notice] = "削除しました"
    redirect_to accounts_path
  end
  
  def category
    @accounts = Account.where(category_id:params[:category_id])
    @account = Account.find_by(category_id:params[:category_id])
    sum
  end
  
  def months
  end
  
  def show_month
    @accounts = Account.where(month:params[:month].to_i, user_id: current_user.id)
    @account = Account.find_by(month:params[:month])
    sum
  end
  
  
  
  private
  
    def set_account
      @account = Account.find_by(id: params[:id])
    end
    
    def change_account
      @account.year = params[:year]
      @account.month = params[:month]
      @account.date = params[:date]
      @account.week = params[:week]
    end

    def account_params
      params.require(:account).permit(:year, :month, :date, :week, :money, 
                                    :category_id, :body, :user_id)
    end
    
    def correct_user_account
      @account = Account.find_by(id: params[:id])
      if current_user.id != @account.user_id
        flash[:notice] = "権限がありません"
        redirect_to root_url
      end
    end
end
