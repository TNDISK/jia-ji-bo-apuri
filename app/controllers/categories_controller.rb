class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user
  before_action :correct_user_category, only: [:show, :edit, :update, :destroy]
  

  def index
    @categories = Category.where(user_id: current_user.id)
  end

  def new
    @category = current_user.categories.build
  end
  
  def create
    @category = current_user.categories.build(category_params)
    if @category.save
      flash[:notice] = "カテゴリを作成しました"
      redirect_to @category
    else
      render 'new'
    end
  end
  
  def show
    @accounts = @category.accounts
    sum
  end

  def edit
  end
  
  def update
    if @category.update(category_params)
      flash[:notice] = "編集しました"
      redirect_to @category
    else
      render 'edit'
    end
  end
  
  def destroy
    @category.destroy
    flash[:notice] = "削除しました"
    redirect_to categories_path
  end
  
  private
  
    def set_category
      @category = Category.find(params[:id])
    end
  
    def category_params
      params.require(:category).permit(:title, :body, :user_id)
    end
    
    def correct_user_category
      @category = Category.find(params[:id])
      if current_user.id != @category.user_id
        flash[:notice] = "権限がありません"
        redirect_to root_url
      end
    end
end
