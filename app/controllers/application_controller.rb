class ApplicationController < ActionController::Base
  include SessionsHelper
  include AccountsHelper
  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end
  
  private
  
    def logged_in_user
      unless logged_in?
        flash[:notice] = "ログインしてください"
        redirect_to login_url
      end
    end
end
