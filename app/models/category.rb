class Category < ApplicationRecord
  default_scope -> {order(created_at: :desc)}
  validates :title, presence: true
  validates :body, length: { maximum: 140 }
  has_many :accounts, dependent: :destroy
  belongs_to :user
end
