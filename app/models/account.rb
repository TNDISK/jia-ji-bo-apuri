class Account < ApplicationRecord
  default_scope -> {order(created_at: :desc)}
  validates :money, presence: true, numericality: { only_integer: true }
  validates :body, length: { maximum: 50 }
  belongs_to :category
  belongs_to :user
  
  def self.search(search) 
    if search
      where(['body LIKE ?', "%#{search}%"])
    else
      all 
    end
  end
end
