class AddYearToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :year, :string
  end
end
