class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :month
      t.string :date
      t.string :week
      t.integer :money

      t.timestamps
    end
  end
end
