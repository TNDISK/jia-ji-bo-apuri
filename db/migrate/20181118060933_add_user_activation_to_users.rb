class AddUserActivationToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :user_activation, :boolean, deafault: false
  end
end
