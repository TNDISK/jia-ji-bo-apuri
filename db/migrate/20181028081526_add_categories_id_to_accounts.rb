class AddCategoriesIdToAccounts < ActiveRecord::Migration[5.2]
  def change
    add_column :accounts, :category_id, :integer
  end
end
