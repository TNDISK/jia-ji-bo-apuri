Rails.application.routes.draw do
  get "/about" => 'home#about'
  get "/login" => "sessions#new"
  post "/login" => "sessions#create"
  delete '/logout',  to: 'sessions#destroy'

  get "/signup" => "users#new"
  root 'accounts#index'
  get "months" => "accounts#months"
  get "months/:month" => "accounts#show_month"
  get "accounts/category/:category_id" => "accounts#category"
  resources :categories
  resources :accounts
  resources :users, only: [:show, :create, :edit, :update, :destroy]
end
