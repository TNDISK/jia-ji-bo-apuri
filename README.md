# README

Railsを始めて4ヶ月目で作った家計簿アプリです。

ただ家計簿を付けるだけでなくそれをカテゴリーごとに分けたり、月ごとに見られるようにしました。

本番環境URL
https://tanos-account-app.herokuapp.com

細かな使い方
https://tanos-account-app.herokuapp.com/about
